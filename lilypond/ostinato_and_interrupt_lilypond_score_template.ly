\version "2.18.2"
\paper {
  top-system-spacing =
  #'((basic-distance . 15 )
  (minimum-distance . 15 )
  (padding . 0 )
  (stretchability . 0))

  #(set-paper-size "a4" 'portrait)
  system-system-spacing =
  #'((basic-distance . 24)
  (minimum-distance . 24)
  (padding . 0)
  (stretchability . 0))
  
  min-systems-per-page = 8
  max-systems-per-page = 8
  
  print-page-number = ##t
  oddHeaderMarkup = \markup \fill-line { " " }
  evenHeaderMarkup = \markup \fill-line { " " }
  oddFooterMarkup = \markup {
  \fill-line {
  \on-the-fly #not-first-page 
  \concat { 
  "-"
      \fontsize #1.5
      \on-the-fly #print-page-number-check-first
      \fromproperty #'page:page-number-string
   "-"
      }
    }
  }
  evenFooterMarkup = \markup {
  \on-the-fly #not-first-page 
    \fill-line {
  \concat { 
  "-"
      \fontsize #1.5
      \on-the-fly #print-page-number-check-first
      \fromproperty #'page:page-number-string
   "-"
      }
    }
  }
}
\header {
  title = \markup { \normal-text \italic {ostinato and interrupt}}
  piece = "part"
  opus = \markup { \concat {"version generated: "} #(strftime "%Y.%m.%d" (localtime (current-time)))}
  composer = "michael winter (mexico city, mx; 2017)"
  tagline = ""
} 
#(set-global-staff-size 16)
\layout {
  indent = 0.0\cm
  ragged-right = ##t
  \context {
    \Staff
      \override StaffSymbol.line-count = #6
      \override StaffSymbol.staff-space = #1.8
      \override Beam.positions = #'(-5 . -5)
      \override Stem.direction = #DOWN
      \override Stem.stemlet-length = #1
      \override Beam.breakable = ##t
      \remove "Clef_engraver"
      \remove "Time_signature_engraver"
    }
}

#(define-public ((color-staff-lines . rest) grob)

   (define (index-cell cell dir)
     (if (equal? dir RIGHT)
         (cdr cell)
         (car cell)))

   (define (index-set-cell! x dir val)
     (case dir
       ((-1) (set-car! x val))
       ((1) (set-cdr! x val))))

   (let* ((common (ly:grob-system grob))
          (span-points '(0 . 0))
          (thickness (* (ly:grob-property grob 'thickness 1.0)
                        (ly:output-def-lookup (ly:grob-layout grob) 'line-thickness)))
          (width (ly:grob-property grob 'width))
          (line-positions (ly:grob-property grob 'line-positions))
          (staff-space (ly:grob-property grob 'staff-space 1))
          (line-stencil #f)
          (total-lines empty-stencil)
          ;; use a local copy of colors list, since
          ;; stencil creation mutates list
          (colors rest))

     (for-each
      (lambda (dir)
        (if (and (= dir RIGHT)
                 (number? width))
            (set-cdr! span-points width)
            (let* ((bound (ly:spanner-bound grob dir))
                   (bound-ext (ly:grob-extent bound bound X)))
              
              (index-set-cell! span-points dir
                               (ly:grob-relative-coordinate bound common X))
              (if (and (not (ly:item-break-dir bound))
                       (not (interval-empty? bound-ext)))
                  (index-set-cell! span-points dir 
                                   (+ (index-cell span-points dir)
                                      (index-cell bound-ext dir))))))
        (index-set-cell! span-points dir (- (index-cell span-points dir)
                                            (* dir thickness 0.5))))
      (list LEFT RIGHT))

     (set! span-points
           (coord-translate span-points
                            (- (ly:grob-relative-coordinate grob common X))))
     (set! line-stencil
           (make-line-stencil thickness (car span-points) 0 (cdr span-points) 0))

     (if (pair? line-positions)
         (for-each (lambda (position)
                     (let ((color (if (pair? colors)
                                      (car colors)
                                      #f)))
                       (set! total-lines
                             (ly:stencil-add
                              total-lines
                              (ly:stencil-translate-axis
                               (if (color? color)
                                   (ly:stencil-in-color line-stencil
                                                        (first color)
                                                        (second color)
                                                        (third color))
                                   line-stencil)
                               (* position staff-space 0.5) Y)))
                       (and (pair? colors)
                            (set! colors (cdr colors)))))
                   line-positions)       
         (let* ((line-count (ly:grob-property grob 'line-count 5))
                (height (* (1- line-count) (/ staff-space 2))))
           (do ((i 0 (1+ i)))                      
               ((= i line-count))
             (let ((color (if (and (pair? colors)
                                   (> (length colors) i))
                              (list-ref colors i)
                              #f)))
               (set! total-lines (ly:stencil-add
                                  total-lines
                                  (ly:stencil-translate-axis
                                   (if (color? color)
                                       (ly:stencil-in-color line-stencil
                                                            (first color)
                                                            (second color)
                                                            (third color))
                                       line-stencil)
                                   (- height (* i staff-space)) Y)))))))
     total-lines))

#(define-public (bracket-stencils grob)
  (let ((lp (grob-interpret-markup grob (markup #:fontsize 3.5 #:translate (cons -0.3 -0.5) "[")))
        (rp (grob-interpret-markup grob (markup #:fontsize 3.5 #:translate (cons -0.3 -0.5) "]"))))
    (list lp rp)))

bracketify = #(define-music-function (parser loc arg) (ly:music?)
   (_i "Tag @var{arg} to be parenthesized.")
#{
  \once \override ParenthesesItem.stencils = #bracket-stencils
  \parenthesize $arg
#})

{
  \new Score 
  \with {
    \remove "Bar_number_engraver"
    proportionalNotationDuration = #(ly:make-moment 1 16)
  }
  <<
  \new Staff
  \with { 
    \remove "Stem_engraver"
  }
  %<<music>>
  >>
}